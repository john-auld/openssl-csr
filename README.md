# Create csr request with SAN's using openssl #

This BASH script creates csr requests with Subject Alternative Names (SAN's) using openssl.

Csr's can be verified using the example below.

```
openssl req -verify -noout -text -in www.example.com.csr
```
